"""Taylor Green vortex flow (5 minutes).
"""
import numpy as np
from pysph.examples import taylor_green as TG

from dtsph import DTSPHScheme

U, L = TG.U, TG.L


class TaylorGreen(TG.TaylorGreen):

    def consume_user_options(self):
        nx = self.options.nx
        re = self.options.re
        self.nu = nu = U * L / re
        self.dx = dx = L / nx
        self.volume = dx * dx
        self.hdx = self.options.hdx

        h0 = self.hdx * self.dx
        dt_cfl = 0.25 * h0 / U
        dt_viscous = 0.125 * h0**2 / nu
        dt_force = 0.25 * 1.0

        self.dt = min(dt_cfl, dt_viscous, dt_force)
        self.tf = 5.0
        self.kernel_correction = self.options.kernel_correction
        # Trick parent class into working correctly when creating particles.
        self.options.scheme = 'dtsph'

    def configure_scheme(self):
        scheme = self.scheme
        pfreq = 10
        scheme.configure(nu=self.nu, h=self.hdx*self.dx)
        scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=pfreq,
            output_at_times=[0.2, 0.4, 0.8, 1.0]
        )

    def create_scheme(self):
        beta = 5.0
        cs = beta*U
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=None, dtaufac=0.2, cs=cs,
            vref=U, rho=TG.rho0, debug=False, has_ghosts=True, weak=False,
            h=None
        )
        return dtsph

    def create_particles(self):
        [fluid] = super(TaylorGreen, self).create_particles()
        b = -8.0*np.pi*np.pi / self.options.re
        up, vp, pp = TG.exact_solution(U=1.0, b=b, t=-self.dt, x=fluid.x,
                                       y=fluid.y)
        fluid.un_p[:] = up
        fluid.vn_p[:] = vp
        fluid.wn_p[:] = 0.0
        fluid.pn_p[:] = pp
        nfp = fluid.get_number_of_particles()
        fluid.orig_idx[:] = np.arange(nfp)
        fluid.add_output_arrays(['orig_idx'])
        return [fluid]

    def create_tools(self):
        tools = []
        options = self.options
        if options.remesh > 0:
            if options.remesh_eq == 'm4':
                equations = [TG.M4(dest='interpolate', sources=['fluid'])]
            else:
                equations = None
            from pysph.solver.tools import SimpleRemesher
            props = ['u', 'v', 'p']
            remesher = SimpleRemesher(
                self, 'fluid', props=props,
                freq=self.options.remesh, equations=equations
            )
            tools.append(remesher)

        if options.shift_freq > 0:
            from pysph.sph.wc.shift import ShiftPositions
            shift = ShiftPositions(
                self, 'fluid', freq=self.options.shift_freq,
                shift_kind=self.options.shift_kind,
                correct_velocity=self.options.correct_vel,
                parameter=self.options.shift_parameter
            )
            tools.append(shift)

        return tools


if __name__ == '__main__':
    app = TaylorGreen()
    app.run()
    app.post_process(app.info_filename)
