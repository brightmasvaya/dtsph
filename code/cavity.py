"""Cavity with DTSPH
"""
from pysph.examples.cavity import LidDrivenCavity as Cavity
from dtsph import DTSPHScheme


# domain and reference values
L = 1.0
Umax = 1.0
c0 = 10 * Umax
rho0 = 1.0
p0 = c0 * c0 * rho0


class CavityDTSPH(Cavity):
    def consume_user_options(self):
        self.hdx = 1.0
        self.options.scheme = 'dtsph'
        super(CavityDTSPH, self).consume_user_options()

    def configure_scheme(self):
        dt = 0.25 * self.hdx * self.dx / Umax
        self.scheme.configure(nu=self.nu, h=self.hdx*self.dx)
        self.scheme.configure_solver(tf=self.tf, dt=dt, pfreq=10)

    def create_scheme(self):
        scheme = DTSPHScheme(
            ['fluid'], ['solid'], dim=2, nu=0.0, cs=c0, dtaufac=0.1,
            vref=Umax, rho=rho0, alpha=0.0, debug=False, h=None
        )
        return scheme

    def create_particles(self):
        [fluid, solid] = super(CavityDTSPH, self).create_particles()

        solid.u[solid.x > 1.0] = 0.0
        # Set previous step velocities to zero.
        fluid.un_p[:] = 0.0
        fluid.un_p[:] = 0.0

        return [fluid, solid]


if __name__ == '__main__':
    app = CavityDTSPH()
    app.run()
    app.post_process(app.info_filename)
