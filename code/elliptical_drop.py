import numpy as np
from numpy import pi, cos, sin
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Group
from pysph.examples.elliptical_drop import EllipticalDrop
from pysph.sph.scheme import SchemeChooser
from pysph.sph.wc.edac import EDACScheme
from dtsph import DTSPHScheme, ShiftPosition, IdentifyFreeSurface
from integrator_step import EDACStepShift


class DTEllipticalDrop(EllipticalDrop):
    def initialize(self):
        super(DTEllipticalDrop, self).initialize()
        self.hdx = 1.2
        self.vref = np.sqrt(2.0)*100
        self.co = self.vref*5.0

    def configure_scheme(self):
        if self.options.scheme == 'dtsph':
            dt = 0.25*self.hdx*self.dx/self.vref
            tf = 0.0076
            print(self.hdx*self.dx, self.co)
            self.scheme.configure_solver(
                dt=dt, tf=tf, adaptive_timestep=False, pfreq=10,
                output_at_times=[0.0008, 0.0038]
            )
        elif self.options.scheme == 'edac':
            self.scheme.configure(
                h=self.hdx*self.dx
            )
            dt = 0.25*self.hdx*self.dx/(141 + self.co)
            tf = 0.0076
            self.scheme.configure_solver(
                dt=dt, tf=tf, adaptive_timestep=False,
                output_at_times=[0.0008, 0.0038],
                extra_steppers={'fluid': EDACStepShift()}
            )

    def create_scheme(self):
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=0.0, cs=self.co,
            dtaufac=0.1, vref=self.vref, rho=self.ro, alpha=0.1, debug=False,
            h=self.hdx*self.dx
        )
        edac = EDACScheme(
            fluids=['fluid'], solids=[], dim=2, c0=self.co, nu=0.0,
            h=self.hdx*self.dx, rho0=self.ro, pb=0.0, eps=0.0, clamp_p=False
        )

        s = SchemeChooser(default='dtsph', dtsph=dtsph, edac=edac)
        return s

    def create_equations(self):
        equations = self.scheme.get_equations()
        if self.options.scheme == 'edac':
            shift = [
                Group(equations=[
                    IdentifyFreeSurface(dest='fluid', sources=['fluid'])
                ]),
                Group(equations=[
                    ShiftPosition(dest='fluid', sources=['fluid'])
                ]),
            ]
            equations.extend(shift)
        return equations

    def create_particles(self):
        """Create the circular patch of fluid."""
        dx = self.dx
        hdx = self.hdx
        co = self.co
        ro = self.ro
        name = 'fluid'
        r = np.arange(dx/2, 1+dx, dx)
        x, y = np.array([]), np.array([])
        for i in r:
            spacing = dx
            theta = np.linspace(0, 2*pi, int(2*pi*i/spacing), endpoint=False)
            x = np.append(x,  i * cos(theta))
            y = np.append(y,  i * sin(theta))

        m = dx*dx*ro
        h = hdx*dx
        rho = ro
        u = -100*x
        v = 100*y

        pa = get_particle_array(x=x, y=y, m=m, rho=rho, h=h, u=u, v=v,
                                name=name)

        print("Elliptical drop :: %d particles"
              % (pa.get_number_of_particles()))
        mu = ro*self.alpha*hdx*dx*co/8.0
        print("Effective viscosity: rho*alpha*h*c/8 = %s" % mu)

        self.scheme.setup_properties([pa])
        if self.options.scheme == 'dtsph':
            pa.un_p[:] = pa.u
            pa.vn_p[:] = pa.v
        elif self.options.scheme == 'dtsph':
            # One must set the pessure to zero, otherwise the particles will
            # just go apart due to the pressure.
            pa.p[:] = 0.0

        if self.options.scheme in ['edac', 'wcsph', 'iisph']:
            pa.add_property('div_r')
            pa.add_property('shift_x')
            pa.add_property('shift_y')
            pa.add_property('shift_z')
        return [pa]


if __name__ == '__main__':
    app = DTEllipticalDrop()
    app.run()
    app.post_process(app.info_filename)
