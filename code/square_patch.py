from __future__ import print_function

import os
import numpy as np

# PySPH base and carray imports
from pysph.base.utils import get_particle_array
from pysph.base.kernels import QuinticSpline

# PySPH solver and integrator
from pysph.sph.equation import Group
from pysph.solver.application import Application
from pysph.sph.scheme import WCSPHScheme, SchemeChooser
from pysph.sph.iisph import IISPHScheme
from pysph.sph.wc.edac import EDACScheme
from dtsph import DTSPHScheme, ShiftPosition, IdentifyFreeSurface
from integrator_step import EDACStepShift


def initial_p(x, y, L=1.0, rho=1.0, omega=1.0):
    xs = x + L*0.5
    ys = y + L*0.5
    pi = np.pi
    p = 0.0
    n_max = 10
    for m in range(1, n_max, 2):
        for n in range(1, n_max, 2):
            k = -rho*32*omega*omega/(n*m*pi*pi*((n*pi/L)**2 + (m*pi/L)**2))
            p += k*np.sin(m*pi*xs/L)*np.sin(n*pi*ys/L)
    return p


class SquarePatch(Application):
    def initialize(self):
        self.omega = 1.0
        self.u0 = 0.5*np.sqrt(2)*self.omega
        self.c0 = 10.0*self.u0
        self.rho0 = 1.0
        self.hdx = 1.3
        self.dx = 0.02

    def add_user_options(self, group):
        group.add_argument(
            "--nx", action="store", type=int, dest="nx", default=50,
            help="Number of points along x direction. (default 50)"
        )

    def consume_user_options(self):
        self.nx = self.options.nx
        self.dx = 1.0/self.nx

    def create_scheme(self):
        h0 = self.hdx*self.dx
        wcsph = WCSPHScheme(
            ['fluid'], [], dim=2, rho0=self.rho0, c0=self.c0,
            h0=h0, hdx=self.hdx, gamma=7.0, alpha=0.15, beta=0.0
        )
        edac = EDACScheme(
            ['fluid'], [], dim=2, rho0=self.rho0, c0=self.c0, pb=0.0,
            h=h0, nu=0.0, eps=0.0
        )
        iisph = IISPHScheme(
            ['fluid'], [], dim=2, rho0=self.rho0,
        )
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=[], dim=2, nu=0.0, dtaufac=0.1,
            cs=self.c0, vref=self.u0, rho=self.rho0, alpha=0.15, debug=False,
            h=h0
        )
        s = SchemeChooser(default='dtsph', wcsph=wcsph,
                          edac=edac, iisph=iisph, dtsph=dtsph)
        return s

    def create_equations(self):
        equations = self.scheme.get_equations()
        if self.options.scheme == 'edac':
            shift = [
                Group(equations=[
                    IdentifyFreeSurface(dest='fluid', sources=['fluid'])
                ]),
                Group(equations=[
                    ShiftPosition(dest='fluid', sources=['fluid'])
                ]),
            ]
            equations.extend(shift)
        return equations

    def configure_scheme(self):
        scheme = self.scheme
        kernel = QuinticSpline(dim=2)
        tf = 3.0
        dt = 0.25*self.hdx*self.dx/(self.u0 + self.c0)
        if self.options.scheme == 'wcsph':
            scheme.configure(h0=self.hdx*self.dx)
            scheme.configure_solver(
                kernel=kernel, dt=dt, tf=tf,
                adaptive_timestep=False,
                output_at_times=[1.0, 2.0, 3.0]
            )
        elif self.options.scheme == 'edac':
            scheme.configure_solver(
                kernel=kernel,
                dt=dt, tf=tf,
                adaptive_timestep=False,
                output_at_times=[1.0, 2.0, 3.0],
                extra_steppers={'fluid': EDACStepShift()}
            )
        elif self.options.scheme in ['iisph', 'dtsph']:
            dt *= 10
            scheme.configure_solver(
                kernel=kernel, dt=dt, tf=tf,
                adaptive_timestep=False,
                output_at_times=[1.0, 2.0, 3.0]
            )

    def create_particles(self):
        """Create the circular patch of fluid."""
        dx = self.dx
        hdx = self.hdx
        ro = self.rho0
        nx = self.nx
        x, y = np.mgrid[-0.5:0.5:nx*1j, -0.5:0.5:nx*1j]
        x = x.ravel()
        y = y.ravel()

        u = y*self.omega
        v = -x*self.omega
        p = initial_p(x, y)

        pa = get_particle_array(
            name='fluid', x=x, y=y, m=dx*dx*ro, rho=ro, h=hdx*dx, p=p,
            u=u, v=v
        )

        self.scheme.setup_properties([pa])
        if self.options.scheme in ['edac', 'wcsph', 'iisph']:
            pa.add_property('div_r')
            pa.add_property('shift_x')
            pa.add_property('shift_y')
            pa.add_property('shift_z')
        if self.options.scheme == 'dtsph':
            pa.un_p[:] = u
            pa.vn_p[:] = v
        return [pa]

    def _make_final_plot(self):
        try:
            import matplotlib
            matplotlib.use('Agg')
            from matplotlib import pyplot as plt
        except ImportError:
            print("Post processing requires matplotlib.")
            return
        last_output = self.output_files[-1]
        from pysph.solver.utils import load
        data = load(last_output)
        pa = data['arrays']['fluid']
        tf = data['solver_data']['t']
        plt.scatter(pa.x, pa.y, marker='.')
        plt.ylim(-2, 2)
        plt.xlim(plt.ylim())
        plt.title("Particles at %s secs" % tf)
        plt.xlabel('x')
        plt.ylabel('y')
        fig = os.path.join(self.output_dir, "final.png")
        plt.savefig(fig, dpi=300)
        print("Figure written to %s." % fig)

    def post_process(self, info_file_or_dir):
        if self.rank > 0:
            return
        self.read_info(info_file_or_dir)
        if len(self.output_files) == 0:
            return
        self._make_final_plot()


if __name__ == '__main__':
    app = SquarePatch()
    app.run()
    app.post_process(app.info_filename)
