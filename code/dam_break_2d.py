"""Dam break solved with DTSPH.
"""
import numpy as np

from pysph.base.kernels import QuinticSpline
from pysph.sph.equation import Group
from pysph.sph.scheme import SchemeChooser
from pysph.examples import dam_break_2d as DB
from pysph.tools.geometry import get_2d_tank, get_2d_block
from pysph.base.utils import get_particle_array
from pysph.sph.wc.edac import EDACScheme

from dtsph import DTSPHScheme, ShiftPosition, IdentifyFreeSurface
from integrator_step import EDACStepShift

fluid_column_height = 2.0
fluid_column_width = 1.0
container_height = 4.0
container_width = 4
nboundary_layers = 4

g = 9.81
h = 0.0390
ro = 1000.0
vref = np.sqrt(2*g*fluid_column_height)
co = 10.0 * vref
nu = 0.0
tf = 1.0
edac_nu = 0.5*h*co/8


class Dambreak2D(DB.DamBreak2D):
    def create_particles(self):
        xt, yt = get_2d_tank(dx=self.dx, length=container_width,
                             height=container_height, num_layers=4)
        xf, yf = get_2d_block(dx=self.dx, length=fluid_column_width,
                              height=fluid_column_height, center=[-1.5, 1])
        xt += 2
        xf += 2
        xf += self.dx
        yf += self.dx
        self.h = h = self.hdx * self.dx
        m = self.dx**2 * ro
        fluid = get_particle_array(name='fluid', x=xf, y=yf, h=h, m=m, rho=ro)
        tank = get_particle_array(name='boundary', x=xt, y=yt, h=h, m=m,
                                  rho=ro)
        self.scheme.setup_properties([fluid, tank])
        if self.options.scheme in ['edac', 'wcsph', 'iisph']:
            fluid.add_property('div_r')
            fluid.add_property('shift_x')
            fluid.add_property('shift_y')
            fluid.add_property('shift_z')
        return [fluid, tank]

    def consume_user_options(self):
        self.hdx = 1.0
        super(Dambreak2D, self).consume_user_options()

    def configure_scheme(self):
        if self.options.scheme == 'dtsph':
            dt = 0.25*self.h/vref
            print("dt = %f" % dt)
            self.scheme.configure(h=self.h)
            self.scheme.configure_solver(
                dt=dt, tf=tf, adaptive_timestep=False, pfreq=10,
                output_at_times=[0.4, 0.6, 0.8, 1.0]
            )
        elif self.options.scheme == 'edac':
            kw = dict(
                tf=tf, output_at_times=[0.4, 0.6, 0.8, 1.0]
            )
            self.scheme.configure(h=self.h)
            kernel = QuinticSpline(dim=2)
            dt = 0.125 * self.h / co
            kw.update(dict(kernel=kernel, dt=dt,
                           extra_steppers={'fluid': EDACStepShift()}))
            print("dt = %f" % dt)
            self.scheme.configure_solver(**kw)

    def create_scheme(self):
        edac = EDACScheme(
            fluids=['fluid'], solids=['boundary'], dim=2, c0=co, nu=nu,
            rho0=ro, h=h, pb=0.0, gy=-g, eps=0.0, clamp_p=True
        )
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=['boundary'], dim=2, nu=nu, dtaufac=0.2,
            cs=co, vref=vref, rho=ro, alpha=0.25, gy=-g, h=None
        )
        s = SchemeChooser(default='dtsph', edac=edac, dtsph=dtsph)
        return s

    def create_equations(self):
        equations = self.scheme.get_equations()
        if self.options.scheme == 'edac':
            shift = [
                Group(equations=[
                    IdentifyFreeSurface(dest='fluid',
                                        sources=['fluid', 'boundary'])
                ]),
                Group(equations=[
                    ShiftPosition(dest='fluid', sources=['fluid', 'boundary'])
                ]),
            ]
            equations.extend(shift)
        return equations


if __name__ == '__main__':
    app = Dambreak2D()
    app.run()
    app.post_process(app.info_filename)
