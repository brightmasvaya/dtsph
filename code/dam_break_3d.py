import numpy as np
from pysph.base.kernels import QuinticSpline
from pysph.examples import dam_break_3d as DB
from pysph.sph.equation import Group
from pysph.sph.scheme import SchemeChooser
from pysph.sph.wc.edac import EDACScheme

from dtsph import DTSPHScheme, ShiftPosition, IdentifyFreeSurface


dim = 3
g = 9.81
tf = 6.0

# parameter to change the resolution
dx = 0.02
nboundary_layers = 4
hdx = 1.3
ro = 1000.0
h0 = dx * hdx
gamma = 7.0
alpha = 0.25
beta = 0.0


class DamBreak3D(DB.DamBreak3D):
    def initialize(self):
        self.vref = np.sqrt(2.0*g*0.55)
        self.co = 10*self.vref

    def consume_user_options(self):
        dx = self.options.dx
        self.dx = dx
        self.geom = DB.DamBreak3DGeometry(
            dx=dx, nboundary_layers=nboundary_layers, hdx=hdx, rho0=ro
        )
        self.vref = self.geom.get_max_speed(g=g)
        self.co = 10.0 * self.vref

    def create_particles(self):
        [fluid, boundary, obstacle] = self.geom.create_particles()

        self.scheme.setup_properties([fluid, boundary, obstacle])
        if self.options.scheme in ['edac', 'wcsph', 'iisph']:
            fluid.add_property('div_r')
            fluid.add_property('shift_x')
            fluid.add_property('shift_y')
            fluid.add_property('shift_z')
        return [fluid, boundary, obstacle]

    def configure_scheme(self):
        h0 = self.dx * hdx
        if self.options.scheme == 'dtsph':
            dt = 0.25*h0/self.vref
            print("dt = %f" % dt)
            self.scheme.configure(cs=self.co, vref=self.vref, h=h0)
            self.scheme.configure_solver(
                dt=dt, tf=tf, adaptive_timestep=False, pfreq=10,
                output_at_times=[0.4, 0.6, 1.0]
            )
        elif self.options.scheme == 'edac':
            kw = dict(
                tf=tf, output_at_times=[0.4, 0.6, 1.0]
            )
            self.scheme.configure(h=h0)
            kernel = QuinticSpline(dim=dim)
            dt = 0.25 * h0 / self.co
            kw.update(dict(kernel=kernel, dt=dt, pfreq=20,))
            print("dt = %f" % dt)
            self.scheme.configure_solver(**kw)

    def create_scheme(self):
        edac = EDACScheme(
            fluids=['fluid'], solids=['boundary', 'obstacle'], dim=dim,
            c0=self.co, nu=0.0, rho0=ro, h=h0, pb=0.0, gz=-g, eps=0.0,
            clamp_p=True, alpha=alpha
        )
        dtsph = DTSPHScheme(
            fluids=['fluid'], solids=['boundary', 'obstacle'], dim=dim, nu=0.0,
            dtaufac=0.1, cs=self.co, vref=self.vref, rho=ro, alpha=alpha, gz=-g,
            h=None
        )
        s = SchemeChooser(default='dtsph', edac=edac, dtsph=dtsph)
        return s


if __name__ == '__main__':
    app = DamBreak3D()
    app.run()
