"""
Dual-timestep SPH

"""
from __future__ import print_function
import os
from math import sqrt
import numpy as np
from compyle.api import declare
from pysph.sph.integrator_step import IntegratorStep
from pysph.base.utils import get_particle_array
from pysph.sph.equation import Equation, Group
from pysph.sph.scheme import Scheme, add_bool_argument
from pysph.sph.integrator import Integrator
from pysph.base.particle_array import get_ghost_tag
from corrections import (GradientCorrectionPreStep, GradientCorrection)


GHOST_TAG = get_ghost_tag()


def printf(*args): pass


def get_particle_array_dtsph(constants=None, **props):
    dtsph_props = [
        'utk', 'vtk', 'wtk', 'un_p', 'vn_p', 'wn_p', 'pn_p', 'ap',
        'u0', 'v0', 'w0', 'x0', 'y0', 'z0', 'p0', 'vmagt', 'V',
        'wij', 'shift_x', 'shift_y', 'shift_z',
        'div_r'
    ]

    pa = get_particle_array(
        additional_props=dtsph_props, constants=constants, **props
    )
    # FixMe: Use the max_iteration to set this value.
    pa.add_constant('err_pt', np.zeros(1001, dtype=np.float))
    pa.add_constant('err_vt', np.zeros(1001, dtype=np.float))
    pa.add_output_arrays(['p', 'un_p', 'vn_p', 'ap', 'vmagt'])
    pa.add_output_arrays(['shift_x', 'shift_y', 'shift_z', 'div_r'])
    return pa


class DTSPHIntegrator(Integrator):
    def one_timestep(self, t, dt):
        self.initialize()

        self.compute_accelerations(0)

        self.stage1()
        self.update_domain()

        self.do_post_stage(dt, 1)

    def initial_acceleration(self, t, dt):
        pass


class DTSPHSteadyIntegrator(Integrator):
    def one_timestep(self, t, dt):
        self.initialize()

        self.stage1()
        self.do_post_stage(dt, 1)

        self.compute_accelerations(update_nnps=False)

        self.stage2()
        self.do_post_stage(dt, 2)


class DTSPHStep(IntegratorStep):
    def initialize(self, d_idx, d_u, d_v, d_w, d_u0, d_v0, d_w0, d_x, d_y, d_z,
                   d_x0, d_y0, d_z0, d_p, d_p0):
        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]
        d_p0[d_idx] = d_p[d_idx]

        d_x0[d_idx] = d_x[d_idx]
        d_y0[d_idx] = d_y[d_idx]
        d_z0[d_idx] = d_z[d_idx]

    def stage1(self, d_idx, d_utk, d_vtk, d_wtk, d_un_p, d_vn_p, d_wn_p,
               d_u0, d_v0, d_w0, d_p0, d_pn_p, d_x, d_y, d_z, d_x0,
               d_y0, d_z0, d_u, d_v, d_w, dt, d_shift_x, d_shift_y, d_shift_z):
        d_un_p[d_idx] = d_u0[d_idx]
        d_vn_p[d_idx] = d_v0[d_idx]
        d_wn_p[d_idx] = d_w0[d_idx]
        d_pn_p[d_idx] = d_p0[d_idx]

        d_utk[d_idx] = 0.0
        d_vtk[d_idx] = 0.0
        d_wtk[d_idx] = 0.0

        dtb2 = 0.5*dt
        d_x[d_idx] = d_x0[d_idx] + dtb2 * (d_u[d_idx] + d_u0[d_idx])
        d_y[d_idx] = d_y0[d_idx] + dtb2 * (d_v[d_idx] + d_v0[d_idx])
        d_z[d_idx] = d_z0[d_idx] + dtb2 * (d_w[d_idx] + d_w0[d_idx])

        d_x[d_idx] += d_shift_x[d_idx]
        d_y[d_idx] += d_shift_y[d_idx]
        d_z[d_idx] += d_shift_z[d_idx]


class DTSPHSteadyStep(IntegratorStep):
    def initialize(self, d_idx, d_u, d_v, d_w, d_p, d_u0, d_v0, d_w0, d_p0):
        d_u0[d_idx] = d_u[d_idx]
        d_v0[d_idx] = d_v[d_idx]
        d_w0[d_idx] = d_w[d_idx]
        d_p0[d_idx] = d_p[d_idx]

    def stage1(self, d_idx, d_u, d_v, d_w, d_p, d_au, d_av, d_aw, d_ap, dt):
        dtb2 = 0.5*dt
        d_u[d_idx] += dtb2 * d_au[d_idx]
        d_v[d_idx] += dtb2 * d_av[d_idx]
        d_w[d_idx] += dtb2 * d_aw[d_idx]
        d_p[d_idx] += dtb2 * d_ap[d_idx]

    def stage2(self, d_idx, d_u, d_v, d_w, d_p, d_u0, d_v0, d_w0, d_p0,
               d_au, d_av, d_aw, d_ap, dt):
        d_u[d_idx] = d_u0[d_idx] + dt * d_au[d_idx]
        d_v[d_idx] = d_v0[d_idx] + dt * d_av[d_idx]
        d_w[d_idx] = d_w0[d_idx] + dt * d_aw[d_idx]
        d_p[d_idx] = d_p0[d_idx] + dt * d_ap[d_idx]


class AdvectVelocityPressure(Equation):
    r"""Adds a :math:`-\vec{V} \cdot grad(\vec{V})` term to the momentum
        acceleration and a :math:`-\vec{V} \cdot grad(p)` to the pressure.

        This is useful when performing steady state solutions.
    """

    def loop(self, d_idx, d_rho, d_m, d_u, d_v, d_w, d_p, d_au, d_av, d_aw,
             d_ap, s_idx, s_rho, s_m, s_u, s_v, s_w, s_p, DWIJ):
        rhoi1 = 1.0/d_rho[d_idx]
        rhoj1 = 1.0/s_rho[s_idx]
        voli = d_m[d_idx] * rhoi1
        volj = s_m[s_idx] * rhoj1

        uij = (d_u[d_idx]*voli + s_u[s_idx]*volj)
        vij = (d_v[d_idx]*voli + s_v[s_idx]*volj)
        wij = (d_w[d_idx]*voli + s_w[s_idx]*volj)
        pij = (d_p[d_idx]*rhoi1*rhoi1 + s_p[s_idx]*rhoj1*rhoj1)

        v_dot_dwij = -(d_u[d_idx]*DWIJ[0] + d_v[d_idx]*DWIJ[1] +
                       d_w[d_idx]*DWIJ[2])
        d_au[d_idx] += v_dot_dwij*uij
        d_av[d_idx] += v_dot_dwij*vij
        d_aw[d_idx] += v_dot_dwij*wij
        d_ap[d_idx] += s_m[s_idx]*v_dot_dwij*pij


class UpdatePosition(Equation):
    def __init__(self, dest, sources, dtaufac):
        self.dtaufac = dtaufac
        super(UpdatePosition, self).__init__(dest, sources)

    # Do this in post loop as it is reused after PredictPressure
    def post_loop(self, d_idx, d_x, d_y, d_z, d_utk, d_vtk, d_wtk, dt):
        dtb2 = 0.5*self.dtaufac*dt
        d_x[d_idx] += dtb2 * d_utk[d_idx]
        d_y[d_idx] += dtb2 * d_vtk[d_idx]
        d_z[d_idx] += dtb2 * d_wtk[d_idx]


class PredictPressure(Equation):
    def __init__(self, dest, sources, cs):
        self.cs = cs
        self.cs2 = cs*cs
        super(PredictPressure, self).__init__(dest, sources)

    def initialize(self, d_ap, d_idx):
        d_ap[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_m, s_rho, d_ap, DWIJ, VIJ):
        cs2 = self.cs2
        rhoj1 = 1.0/s_rho[s_idx]
        Vj = s_m[s_idx] * rhoj1
        rhoi = d_rho[d_idx]

        vij_dot_dwij = VIJ[0]*DWIJ[0] + VIJ[1]*DWIJ[1] + VIJ[2]*DWIJ[2]

        d_ap[d_idx] += rhoi*Vj*cs2*vij_dot_dwij


class PredictPressureEDAC(Equation):
    def __init__(self, dest, sources, cs, nu):
        self.cs = cs
        self.cs2 = cs*cs
        self.nu = nu
        super(PredictPressureEDAC, self).__init__(dest, sources)

    def initialize(self, d_ap, d_idx):
        d_ap[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_p, d_rho, s_p, s_m, s_rho, d_ap, DWIJ, XIJ,
             R2IJ, EPS):
        rhoij = d_rho[d_idx] + s_rho[s_idx]
        # The viscous damping of pressure.
        xijdotdwij = DWIJ[0]*XIJ[0] + DWIJ[1]*XIJ[1] + DWIJ[2]*XIJ[2]
        tmp = s_m[s_idx] * 4 * self.nu * xijdotdwij / (rhoij*(R2IJ + EPS))
        d_ap[d_idx] += tmp*(d_p[d_idx] - s_p[s_idx])


class PredictPressureAdvectPseudo(PredictPressure):
    # If advect pseudo is true use this in addition to PredictPressure.
    def loop(self, d_idx, s_idx, d_p, d_rho, s_p, d_utk, d_vtk, d_wtk, s_m,
             s_rho, d_ap, DWIJ, VIJ):
        rhoi1 = 1.0/d_rho[d_idx]
        rhoj1 = 1.0/s_rho[s_idx]

        utk = d_utk[d_idx]
        vtk = d_vtk[d_idx]
        wtk = d_wtk[d_idx]
        pij = d_p[d_idx]*rhoi1*rhoi1 + s_p[s_idx]*rhoj1*rhoj1

        vk_dot_dwij = utk*DWIJ[0] + vtk*DWIJ[1] + wtk*DWIJ[2]
        d_ap[d_idx] += s_m[s_idx]*pij*vk_dot_dwij


class SetPressure(Equation):
    def __init__(self, dest, sources, dtaufac):
        self.dtaufac = dtaufac
        super(SetPressure, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_p, d_ap, dt):
        d_p[d_idx] += 0.5 * self.dtaufac * dt * d_ap[d_idx]


class SetPressureSolid(Equation):
    """Set the pressure for solids and use a HG correction
    to ensure that the pressure is not negative.  This uses the Adami BC
    to set an appropriate pressure.

    References
    ----------
    .. [Adami2012] S. Adami et. al "A generalized wall boundary condition for
        smoothed particle hydrodynamics", Journal of Computational Physics
        (2012), pp. 7057--7075.
    .. [Hughes2010] J. P. Hughes and D. I. Graham, "Comparison of
        incompressible and weakly-compressible SPH models for free-surface
        water flows", Journal of Hydraulic Research, 48 (2010), pp. 105-117.
    """
    def __init__(self, dest, sources, gx=0.0, gy=0.0, gz=0.0,
                 hg_correction=True):
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.hg_correction = hg_correction
        super(SetPressureSolid, self).__init__(dest, sources)

    def initialize(self, d_idx, d_p, d_wij):
        d_p[d_idx] = 0.0
        d_wij[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_p, s_p, d_wij, s_rho,
             d_au, d_av, d_aw, WIJ, XIJ):

        # numerator of Eq. (27) ax, ay and az are the prescribed wall
        # accelerations which must be defined for the wall boundary
        # particle
        gdotxij = (self.gx - d_au[d_idx])*XIJ[0] + \
            (self.gy - d_av[d_idx])*XIJ[1] + \
            (self.gz - d_aw[d_idx])*XIJ[2]

        d_p[d_idx] += s_p[s_idx]*WIJ + s_rho[s_idx]*gdotxij*WIJ

        # denominator of Eq. (27)
        d_wij[d_idx] += WIJ

    def post_loop(self, d_idx, d_wij, d_p):
        # extrapolated pressure at the ghost particle
        if d_wij[d_idx] > 1e-14:
            d_p[d_idx] /= d_wij[d_idx]
        if self.hg_correction:
            d_p[d_idx] = max(0.0, d_p[d_idx])


class SolidWallNoSlipBC(Equation):
    r"""Solid wall boundary condition [Adami2012]_
    """

    def __init__(self, dest, sources, nu):
        self.nu = nu
        super(SolidWallNoSlipBC, self).__init__(dest, sources)

    def loop(self, d_idx, s_idx, s_m, d_rho, s_rho,
             d_u, d_v, d_w, s_ug, s_vg, s_wg,
             d_au, d_av, d_aw, DWIJ, XIJ, R2IJ, EPS):
        rhoa = d_rho[d_idx]
        rhob = s_rho[s_idx]

        # scalar part of the kernel gradient
        Fij = DWIJ[0] * XIJ[0] + DWIJ[1] * XIJ[1] + DWIJ[2] * XIJ[2]

        mb = s_m[s_idx]

        tmp = mb * 4 * self.nu * Fij/((rhoa + rhob)*(R2IJ + EPS))

        # accelerations
        d_au[d_idx] += tmp * (d_u[d_idx] - s_ug[s_idx])
        d_av[d_idx] += tmp * (d_v[d_idx] - s_vg[s_idx])
        d_aw[d_idx] += tmp * (d_w[d_idx] - s_wg[s_idx])


class DebugPressure(Equation):
    def __init__(self, dest, sources):
        self.file_ctr = 1
        os.makedirs('debug_output', exist_ok=True)

    def py_initialize(self, dst, t, dt):
        from pysph.solver.utils import dump
        iters = int(t/dt)
        dump(f"debug_output/debug_{iters}_{self.file_ctr}", [dst],
             {'dt': dt, 't': t, 'count': self.file_ctr}, detailed_output=True)
        self.file_ctr += 1


class MomentumEquationPressureGradient(Equation):
    def __init__(self, dest, sources, c0,
                 alpha, beta, gx=0.0, gy=0.0, gz=0.0):
        self.alpha = alpha
        self.beta = beta
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.c0 = c0

        super(MomentumEquationPressureGradient, self).__init__(dest, sources)

    def initialize(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] = 0.0
        d_av[d_idx] = 0.0
        d_aw[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho,
             d_p, d_au, d_av, d_aw, s_m, s_rho, s_p, VIJ,
             XIJ, HIJ, R2IJ, RHOIJ1, EPS, DWIJ):

        rhoi21 = 1.0/(d_rho[d_idx]*d_rho[d_idx])
        rhoj21 = 1.0/(s_rho[s_idx]*s_rho[s_idx])

        vijdotxij = VIJ[0]*XIJ[0] + VIJ[1]*XIJ[1] + VIJ[2]*XIJ[2]

        piij = 0.0
        if vijdotxij < 0:
            muij = (HIJ * vijdotxij)/(R2IJ + EPS)
            piij = -self.alpha*self.c0*muij + self.beta*muij*muij
            piij = piij*RHOIJ1

        tmpi = d_p[d_idx]*rhoi21
        tmpj = s_p[s_idx]*rhoj21

        tmp = (tmpi + tmpj)

        d_au[d_idx] += -s_m[s_idx] * (tmp + piij) * DWIJ[0]
        d_av[d_idx] += -s_m[s_idx] * (tmp + piij) * DWIJ[1]
        d_aw[d_idx] += -s_m[s_idx] * (tmp + piij) * DWIJ[2]

    def post_loop(self, d_idx, d_au, d_av, d_aw):
        d_au[d_idx] += self.gx
        d_av[d_idx] += self.gy
        d_aw[d_idx] += self.gz


class MomentumEquationAdvectPseudo(Equation):
    def loop(self, d_idx, s_idx, d_m, s_m, d_rho, s_rho,
             d_u, d_v, d_w, s_u, s_v, s_w, d_utk, d_vtk, d_wtk,
             d_au, d_av, d_aw, DWIJ):
        rhoi = d_rho[d_idx]
        rhoj = s_rho[s_idx]
        voli = d_m[d_idx] / rhoi
        volj = s_m[s_idx] / rhoj

        uij = (d_u[d_idx]*voli + s_u[s_idx]*volj)
        vij = (d_v[d_idx]*voli + s_v[s_idx]*volj)
        wij = (d_w[d_idx]*voli + s_w[s_idx]*volj)

        vk_dot_dwij = (d_utk[d_idx]*DWIJ[0] + d_vtk[d_idx]*DWIJ[1] +
                       d_wtk[d_idx]*DWIJ[2])
        d_au[d_idx] += vk_dot_dwij*uij
        d_av[d_idx] += vk_dot_dwij*vij
        d_aw[d_idx] += vk_dot_dwij*wij


class GuessState(Equation):
    def post_loop(self, d_idx, d_u, d_v, d_w, d_au, d_av, d_aw, d_x, d_y, d_z,
                  d_p, d_ap, dt):
        dtb2 = dt*0.5
        d_u[d_idx] += dtb2 * d_au[d_idx]
        d_v[d_idx] += dtb2 * d_av[d_idx]
        d_w[d_idx] += dtb2 * d_aw[d_idx]

        d_x[d_idx] += dt * d_u[d_idx]
        d_y[d_idx] += dt * d_v[d_idx]
        d_z[d_idx] += dt * d_w[d_idx]

        d_u[d_idx] += dtb2 * d_au[d_idx]
        d_v[d_idx] += dtb2 * d_av[d_idx]
        d_w[d_idx] += dtb2 * d_aw[d_idx]

        # d_p[d_idx] += dt * d_ap[d_idx]


class UpdatePseudoState(Equation):
    def __init__(self, dest, sources, dtaufac, update_p=False):
        self.dtaufac = dtaufac
        self.update_p = update_p
        super(UpdatePseudoState, self).__init__(dest, sources)

    def post_loop(self, d_idx, d_au, d_av, d_aw, d_ap, d_u, d_v, d_w, d_p,
                  d_u0, d_v0, d_w0, d_p0, d_utk, d_vtk, d_wtk, d_vmagt,
                  d_un_p, d_vn_p, d_wn_p, d_pn_p, dt):
        update_p = self.update_p
        dtau = self.dtaufac*dt
        dt2 = 2*dt
        fac = dt2/(dt2 + 3*dtau)

        uk = d_u[d_idx]
        vk = d_v[d_idx]
        wk = d_w[d_idx]
        pk = d_p[d_idx]

        un = d_u0[d_idx]
        vn = d_v0[d_idx]
        wn = d_w0[d_idx]
        pn = d_p0[d_idx]

        un_p = d_un_p[d_idx]
        vn_p = d_vn_p[d_idx]
        wn_p = d_wn_p[d_idx]
        pn_p = d_pn_p[d_idx]

        d_au_dtau = fac * (d_au[d_idx] - (3*uk - 4*un + un_p) / dt2)
        d_av_dtau = fac * (d_av[d_idx] - (3*vk - 4*vn + vn_p) / dt2)
        d_aw_dtau = fac * (d_aw[d_idx] - (3*wk - 4*wn + wn_p) / dt2)
        d_ap_dtau = fac * (d_ap[d_idx] - (3*pk - 4*pn + pn_p) / dt2)

        d_u[d_idx] += dtau*d_au_dtau
        d_v[d_idx] += dtau*d_av_dtau
        d_w[d_idx] += dtau*d_aw_dtau
        d_p[d_idx] += dtau*d_ap_dtau
        if update_p:
            d_p[d_idx] += dtau*d_ap_dtau

        utk = dt*d_au_dtau
        vtk = dt*d_av_dtau
        wtk = dt*d_aw_dtau
        d_utk[d_idx] = utk

        d_vtk[d_idx] = vtk
        d_wtk[d_idx] = wtk
        if update_p:
            d_ap[d_idx] = d_ap_dtau

        d_vmagt[d_idx] = sqrt(utk*utk + vtk*vtk + wtk*wtk)


class CheckConvergence(Equation):
    def __init__(self, dest, sources, vref, pref, tolerance=1e-3,
                 debug=False, max_iters=1000):
        self.tolerance = tolerance
        self.debug = debug
        self.err_p = 0.0
        self.vt = 0.0
        self.v_diff = 0.0
        self.p_diff = 0.0
        self.n = 3
        self.vref = vref
        self.pref = pref
        self.iters = 0
        self.max_iters = max_iters
        super(CheckConvergence, self).__init__(dest, sources)

    def reduce(self, dst, t, dt):
        import numpy as np
        iters = declare('int')
        iters = self.iters
        if iters == 0:
            dst.err_pt[:] = 0.0
            dst.err_vt[:] = 0.0
        self.err_p = np.mean(np.abs(dst.ap))*dt/self.pref
        self.vt = np.mean(dst.vmagt)/self.vref
        dst.err_pt[iters] = self.err_p
        dst.err_vt[iters] = self.vt
        if iters >= self.n:
            iters += 1
            self.p_diff = np.ptp(dst.err_pt[iters-self.n:iters])
            self.v_diff = np.ptp(dst.err_vt[iters-self.n:iters])
        else:
            self.v_diff = self.vt
            self.p_diff = self.err_p

    def converged(self):
        debug = self.debug
        tolerance = self.tolerance
        err_p = self.err_p
        vt = self.vt
        diff_p = self.p_diff/err_p
        diff_v = self.v_diff/vt
        rtol = 5e-2

        reason = 0  # 0 - converged, 1 - no change, 2 - max iter.
        conv = False
        if (vt < tolerance):
            if (err_p < tolerance):
                reason = 0
                conv = True
            elif (diff_p < rtol):
                conv = True
                reason = 1
        elif (err_p < tolerance) and (diff_v < rtol):
            conv = True
            reason = 1
        elif ((diff_p < rtol) and (diff_v < rtol)):
            conv = True
            reason = 1
        elif self.iters >= self.max_iters:
            conv = True
            reason = 2
        elif (vt > 1.0) or (err_p > 1.0):
            printf("\nERROR: iterations diverging! Reduce your dtau.\n")

        if conv:
            if debug:
                if reason == 0:
                    printf("\rConverged: %d, %g, %g\t\t\n",
                           self.iters, err_p, vt)
                elif reason == 1:
                    printf("\rNo change: %d, %g, %g, %g, %g",
                           self.iters, err_p, vt, diff_p, diff_v)
                elif reason == 2:
                    printf("\nMax iter: %d, %g, %g, %g, %g\t\t\n",
                           self.iters, err_p, vt, diff_p, diff_v)
            self.iters = 0
            self.err_p = 0.0
            self.vt = 0.0
            return 1.0
        else:
            if debug:
                printf("\rNot converged: %d, %g, %g, %g, %g\t\t",
                       self.iters, err_p, vt, diff_p, diff_v)
            self.iters += 1
            return -1


class UpdateGhostProps(Equation):
    def initialize(self, d_idx, d_tag, d_orig_idx, d_rho, d_p, d_u, d_v, d_w,
                   d_utk, d_vtk, d_wtk):
        idx = declare('int')
        if d_tag[d_idx] == 2:
            idx = d_orig_idx[d_idx]
            d_p[d_idx] = d_p[idx]
            d_u[d_idx] = d_u[idx]
            d_v[d_idx] = d_v[idx]
            d_w[d_idx] = d_w[idx]
            d_utk[d_idx] = d_utk[idx]
            d_vtk[d_idx] = d_vtk[idx]
            d_wtk[d_idx] = d_wtk[idx]
            d_rho[d_idx] = d_rho[idx]


class UpdateGhostPressure(Equation):
    def initialize(self, d_idx, d_tag, d_orig_idx, d_p):
        idx = declare('int')
        if d_tag[d_idx] == 2:
            idx = d_orig_idx[d_idx]
            d_p[d_idx] = d_p[idx]


class ShiftPosition(Equation):
    def __init__(self, dest, sources, dim=2, cfl=0.25):
        self.dim = dim
        self.cfl = cfl
        super().__init__(dest, sources)

    def initialize(self, d_idx, d_shift_x, d_shift_y, d_shift_z):
        d_shift_x[d_idx] = 0.0
        d_shift_y[d_idx] = 0.0
        d_shift_z[d_idx] = 0.0

    def loop(self, d_idx, s_idx, s_m, d_shift_x, d_shift_y, d_shift_z, s_rho,
             d_h, d_div_r, WIJ, SPH_KERNEL, DWIJ):
        mj = s_m[s_idx]
        rhoj = s_rho[s_idx]
        Vj = mj/rhoj
        hi = d_h[d_idx]

        WDX = SPH_KERNEL.kernel([0.0, 0.0, 0.0], hi, hi)
        fac = 0.5 * hi * hi
        tmp = fac * Vj * (1 + 0.2 * (WIJ/(WDX))**4)
        if d_div_r[d_idx] > (self.dim-0.5):
            d_shift_x[d_idx] -= tmp * DWIJ[0]
            d_shift_y[d_idx] -= tmp * DWIJ[1]
            d_shift_z[d_idx] -= tmp * DWIJ[2]


class IdentifyFreeSurface(Equation):
    def initialize(self, d_idx, d_div_r):
        d_div_r[d_idx] = 0.0

    def loop(self, d_idx, d_div_r, XIJ, DWIJ, s_m, s_rho, s_idx):
        Vj = s_m[s_idx]/s_rho[s_idx]
        xijdotdwij = XIJ[0]*DWIJ[0] + XIJ[1]*DWIJ[1] + XIJ[2]*DWIJ[2]
        d_div_r[d_idx] -= Vj * xijdotdwij


class SummationDensity(Equation):
    def initialize(self, d_idx, d_rho):
        d_rho[d_idx] = 0.0

    def loop(self, d_idx, s_idx, d_rho, s_m, WIJ):
        d_rho[d_idx] += s_m[s_idx]*WIJ


class CorrectProperties(Equation):
    def initialize(self, d_idx, d_gradv, d_gradp):
        i, j = declare('int', 2)
        for i in range(3):
            d_gradp[d_idx*3 + i] = 0.0
            for j in range(3):
                d_gradv[9*d_idx + 3*j + i] = 0.0

    def loop(self, d_idx, s_idx, s_m, s_rho, d_gradv, d_p, s_p, d_gradp, DWIJ,
             VIJ):
        i, j = declare('int', 2)

        Vj = s_m[s_idx] / s_rho[s_idx]
        pij = d_p[d_idx] - s_p[s_idx]

        for i in range(3):
            d_gradp[d_idx*3 + i] += -Vj * pij * DWIJ[i]
            for j in range(3):
                d_gradv[d_idx*9 + 3*j + i] += -Vj * VIJ[i] * DWIJ[j]

    def post_loop(self, d_idx, d_u, d_v, d_w, d_gradv, d_gradp, d_shift_x, d_shift_y,
                  d_shift_z, d_p):
        res, shift = declare('matrix(3)', 2)
        i, j = declare('int', 2)

        shift[0] = d_shift_x[d_idx]
        shift[1] = d_shift_y[d_idx]
        shift[2] = d_shift_z[d_idx]

        deltap = 0.0
        for i in range(3):
            tmp = 0.0
            deltap += d_gradp[d_idx*3 + i] * shift[i]
            for j in range(3):
                tmp += d_gradv[d_idx*9 + 3*i + j] * shift[j]
            res[i] = tmp

        d_p[d_idx] += deltap
        d_u[d_idx] += res[0]
        d_v[d_idx] += res[1]
        d_w[d_idx] += res[2]


class DTSPHScheme(Scheme):
    def __init__(self, fluids, solids, dim, nu, dtaufac, cs, vref, rho,
                 alpha=0.0, gx=0.0, gy=0.0, gz=0.0, tdamp=0.0,
                 tolerance=1e-3, debug=False, advect_pseudo=False,
                 has_ghosts=False, update_nnps=False, hg_correction=True,
                 max_iters=1000, print_iters=False, weak=False, edac=False,
                 h=None, shift=False, correction=False):
        """The DTSPH scheme

        Parameters
        ----------

        fluids : list(str)
            List of names of fluid particle arrays.
        solids : list(str)
            List of names of solid particle arrays.
        dim: int
            Dimensionality of the problem.
        nu : float
            Kinematic viscosity coefficient.
        cs : float
            Artificial speed of sound.
        dtaufac: float
            Pseudo time iteration step factor (this is multiplied by dt).
        vref: float
            Reference velocity in the flow.
        rho: float
            Reference density.
        alpha: float
            Artificial viscosity.
        gx, gy, gz : float
            Componenents of body acceleration (gravity, external forcing etc.)
        tdamp: float
            Time to damp accelerations up-to.
        tolerance: float
            Tolerance for the convergence of pressure iterations as a fraction.
        debug: bool
            Produce some debugging output on iterations.
        advect_pseudo: bool
            Advection of pressure and velocity in pseudo time (default: false).
        has_ghosts: bool
            The problem has ghost particles so add equations for those.
        update_nnps: bool
            Updates nnps after every iteration.
        hg_correction: bool
            Apply hg correction for the solids.
        max_iters: int
            Max number of iterations to use in the pseudo time.
        print_iterations: int
            Print the output at each iteration, useful for debugging.
        weak: bool
            Use a weakly compressible form for the pressure.
        """
        self.fluids = fluids
        self.solids = solids
        self.dim = dim
        self.nu = nu
        self.cs = cs
        self.dtaufac = dtaufac
        self.alpha = alpha
        self.vref = vref
        self.rho = rho
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.tdamp = tdamp
        self.tolerance = tolerance
        self.debug = debug
        self.advect_pseudo = advect_pseudo
        self.has_ghosts = has_ghosts
        self.update_nnps = update_nnps
        self.hg_correction = hg_correction
        self.max_iters = max_iters
        self.print_iters = print_iters
        self.weak = weak
        self.edac = edac
        self.h = h
        self.shift = shift
        self.correction = correction

    def add_user_options(self, group):
        group.add_argument(
            '--tolerance', action='store', type=float, dest='tolerance',
            default=None,
            help='Tolerance for convergence of iterations as a fraction'
        )
        group.add_argument(
            '--dtsph-cs', action='store', type=float, dest='cs',
            default=None,
            help='Speed of sound in pseudo time.'
        )
        group.add_argument(
            '--dtaufac', action='store', type=float, dest='dtaufac',
            default=None,
            help='Factor to multiply dt to get dtau.'
        )
        group.add_argument(
            '--alpha', action='store', type=float, dest='alpha',
            default=None,
            help='Artificial viscosity.'
        )
        add_bool_argument(
            group, 'debug', dest='debug', default=None,
            help="Produce some debugging output on convergence of iterations."
        )
        add_bool_argument(
            group, 'hg-correction', dest='hg_correction', default=None,
            help="Add HG correction for solid pressure."
        )
        add_bool_argument(
            group, 'dtsph-advect', dest='advect_pseudo', default=None,
            help="Advection of pressure and velocity in pseudo time."
        )
        add_bool_argument(
            group, 'edac', dest='edac', default=None,
            help="Use EDAC correction."
        )
        add_bool_argument(
            group, 'dtsph-update-nnps', dest='update_nnps', default=None,
            help="Updates nnps after every iteration."
        )
        add_bool_argument(
            group, 'dtsph-print-iters', dest='print_iters', default=None,
            help="Dumps output during pseudo-time iterations for debugging."
        )
        add_bool_argument(
            group, 'dtsph-weak', dest='weak', default=None,
            help="Use a weakly compressible formulation."
        )
        add_bool_argument(
            group, 'shift', dest='shift', default=None,
            help="Use a Lind's shifting technique."
        )
        add_bool_argument(
            group, 'correction', dest='correction', default=False,
            help="Add Liu's correction."
        )

    def consume_user_options(self, options):
        vars = ['alpha', 'tolerance', 'debug', 'advect_pseudo', 'cs',
                'update_nnps', 'print_iters', 'weak', 'dtaufac',
                'hg_correction', 'edac', 'shift', 'correction']
        data = dict((var, self._smart_getattr(options, var))
                    for var in vars)
        self.configure(**data)

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        from pysph.base.kernels import QuinticSpline
        from pysph.solver.solver import Solver
        if kernel is None:
            kernel = QuinticSpline(dim=self.dim)

        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        for fluid in self.fluids:
            if fluid not in steppers:
                steppers[fluid] = DTSPHStep()

        cls = DTSPHIntegrator if integrator_cls is None else integrator_cls
        integrator = cls(**steppers)

        self.solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel, **kw
        )

    def _get_edac_nu(self):
        art_nu = 0.5*self.h*self.cs/8
        if art_nu > 0:
            nu = art_nu
            print("Using artificial viscosity for EDAC with nu = %s" % nu)
        else:
            nu = self.nu
            print("Using real viscosity for EDAC with nu = %s" % self.nu)
        return nu

    def _get_momentum_eqs(self, pressure=True):
        from pysph.sph.wc.viscosity import (
            LaminarViscosity,
        )
        all = list(self.fluids) + list(self.solids)

        edac_nu = self._get_edac_nu() if self.edac else None

        eq0 = []
        for fluid in self.fluids:
            if self.correction:
                eq0.append(
                    GradientCorrection(dest=fluid, sources=all)
                )
            eq0.append(
                MomentumEquationPressureGradient(
                    dest=fluid, sources=all, c0=self.cs,
                    alpha=self.alpha, beta=0.0,
                    gx=self.gx, gy=self.gy, gz=self.gz
                )
            )
            if pressure:
                eq0.append(
                    PredictPressure(
                        dest=fluid, sources=all, cs=self.cs
                    )
                )
                if self.advect_pseudo:
                    eq0.append(
                        PredictPressureAdvectPseudo(
                            dest=fluid, sources=all, cs=self.cs
                        )
                    )
                if self.edac:
                    eq0.append(
                        PredictPressureEDAC(
                            dest=fluid, sources=all, cs=self.cs, nu=edac_nu
                        )
                    )
            if self.advect_pseudo:
                # Technically, we should have one term for the ghost
                # solid particles but it is likely we will not use
                # this advection.
                eq0.append(
                    MomentumEquationAdvectPseudo(
                        dest=fluid, sources=all
                    )
                )
            if self.nu > 0.0:
                eq0.append(
                    LaminarViscosity(
                        dest=fluid, sources=self.fluids, nu=self.nu
                    )
                )
                if self.solids:
                    eq0.append(
                        SolidWallNoSlipBC(dest=fluid, sources=self.solids,
                                          nu=self.nu)
                    )
        return eq0

    def _get_pressure_eqs(self):
        all = list(self.fluids) + list(self.solids)
        edac_nu = self._get_edac_nu() if self.edac else None

        eqs = []
        for fluid in self.fluids:
            eqs.append(
                PredictPressure(
                    dest=fluid, sources=all, cs=self.cs
                )
            )
            if self.edac:
                eqs.append(
                    PredictPressureEDAC(
                        dest=fluid, sources=all, cs=self.cs, nu=edac_nu
                    )
                )
            if self.print_iters:
                eqs.append(
                    DebugPressure(dest=fluid, sources=self.fluids)
                )
            if self.advect_pseudo:
                eqs.append(
                    PredictPressureAdvectPseudo(
                        dest=fluid, sources=self.fluids, cs=self.cs
                    )
                )

            if not self.weak:
                eqs.append(
                    SetPressure(dest=fluid, sources=None, dtaufac=self.dtaufac)
                )
            if self.advect_pseudo:
                eqs.append(
                    UpdatePosition(dest=fluid, sources=None,
                                   dtaufac=self.dtaufac)
                )

        groups = [Group(equations=eqs, update_nnps=self.update_nnps)]
        g = self._get_pressure_bc()
        if g is not None:
            groups.append(g)
        return groups

    def _get_pressure_bc(self):
        eqs = []
        for solid in self.solids:
            if not self.weak:
                eqs.append(
                    SetPressureSolid(
                        dest=solid, sources=self.fluids,
                        gx=self.gx, gy=self.gy, gz=self.gz,
                        hg_correction=self.hg_correction
                    )
                )
        if eqs:
            return Group(equations=eqs)
        else:
            return None

    def _get_velocity_bc(self):
        from pysph.sph.wc.transport_velocity import SetWallVelocity
        eqs = [SetWallVelocity(dest=s, sources=self.fluids)
               for s in self.solids]
        return Group(equations=eqs)

    def _get_shift_eqs(self):
        all = self.fluids + self.solids
        shift = []
        for fluid in self.fluids:
            shift.append(Group(equations=[
                IdentifyFreeSurface(dest=fluid, sources=all),
            ]))
        for fluid in self.fluids:
            shift.append(Group(equations=[
                ShiftPosition(dest=fluid, sources=all),
            ]))
        return shift

    def get_equations(self):
        groups = []
        pref = self.rho*self.cs**2

        all = self.fluids + self.solids

        if self.shift:
            correct_eqns = []
            for fluid in self.fluids:
                correct_eqns.append(CorrectProperties(dest=fluid, sources=all))
            groups.append(Group(equations=correct_eqns))

        if self.correction:
            corr = []
            for fluid in self.fluids:
                corr.append(GradientCorrectionPreStep(fluid, all, dim=self.dim))
            groups.append(Group(equations=corr))

        if self.solids:
            groups.append(self._get_velocity_bc())

        eq0 = self._get_momentum_eqs()
        eq0.extend([GuessState(dest=f, sources=None) for f in self.fluids])
        sg0 = Group(equations=eq0, update_nnps=True)
        groups.append(sg0)

        if self.correction:
            corr = []
            for fluid in self.fluids:
                corr.append(GradientCorrectionPreStep(fluid, all, dim=self.dim))
            groups.append(Group(equations=corr))

        solver_equations = []
        p_grp = self._get_pressure_eqs()
        solver_equations.extend(p_grp)

        if not self.update_nnps and self.has_ghosts:
            solver_equations.append(Group(
                equations=[
                    UpdateGhostProps(dest=x, sources=None) for x in self.fluids
                ], real=False
            ))

        if self.solids:
            solver_equations.append(self._get_velocity_bc())

        eq3 = self._get_momentum_eqs(pressure=False)
        eq3.extend([
            UpdatePseudoState(dest=fluid, sources=None,
                              dtaufac=self.dtaufac, update_p=self.weak)
            for fluid in self.fluids
        ])

        solver_equations.append(
            Group(equations=eq3, update_nnps=self.update_nnps)
        )

        p_grp = self._get_pressure_eqs()
        p_grp[0].equations.extend([
            CheckConvergence(
                dest=fluid, sources=self.fluids, vref=self.vref, pref=pref,
                tolerance=self.tolerance, debug=self.debug,
                max_iters=self.max_iters
            )
            for fluid in self.fluids
        ])
        solver_equations.extend(p_grp)

        if not self.update_nnps and self.has_ghosts:
            solver_equations.append(Group(
                equations=[
                    UpdateGhostProps(dest=x, sources=None) for x in self.fluids
                ], real=False
            ))


        equations = groups + [
            Group(equations=solver_equations, iterate=True,
                  max_iterations=self.max_iters, min_iterations=1)
        ]

        if self.shift:
            shift_eqns = self._get_shift_eqs()
            equations.extend(shift_eqns)
        return equations

    def setup_properties(self, particles, clean=True):
        dummy = get_particle_array_dtsph()
        props = set(dummy.properties.keys())
        constants = [dict(name=x, data=v) for x, v in dummy.constants.items()]
        for pa in particles:
            self._ensure_properties(pa, props, clean)
            pa.set_output_arrays(dummy.output_property_arrays)
            for const in constants:
                pa.add_constant(**const)
        particle_arrays = dict([(p.name, p) for p in particles])
        if self.has_ghosts:
            for fluid in self.fluids:
                pa = particle_arrays[fluid]
                pa.add_property('orig_idx', type='int')
        for fluid in self.fluids:
            pa = particle_arrays[fluid]
            pa.add_property('gradv', stride=9)
            pa.add_property('gradp', stride=3)
            pa.add_property('Linv', stride=16)
            pa.add_property('L', stride=16)


        solid_props = ['wij', 'ug', 'vg', 'wg', 'uf', 'vf', 'wf']
        for solid in self.solids:
            pa = particle_arrays[solid]
            for prop in solid_props:
                pa.add_property(prop)


class DTSPHSteadyScheme(Scheme):
    def __init__(self, fluids, solids, dim, nu, cs, h,
                 alpha=0.0, gx=0.0, gy=0.0, gz=0.0, tdamp=0.0,
                 has_ghosts=False, edac=False):
        """The DTSPH scheme

        Parameters
        ----------

        fluids : list(str)
            List of names of fluid particle arrays.
        solids : list(str)
            List of names of solid particle arrays.
        dim: int
            Dimensionality of the problem.
        nu : float
            Kinematic viscosity coefficient.
        cs : float
            Artificial speed of sound.
        alpha: float
            Artificial viscosity.
        gx, gy, gz : float
            Componenents of body acceleration (gravity, external forcing etc.)
        tdamp: float
            Time to damp accelerations up-to.
        """
        self.fluids = fluids
        self.solids = solids
        self.dim = dim
        self.nu = nu
        self.cs = cs
        self.alpha = alpha
        self.gx = gx
        self.gy = gy
        self.gz = gz
        self.tdamp = tdamp
        self.has_ghosts = has_ghosts
        self.edac = edac
        self.h = h

    def add_user_options(self, group):
        group.add_argument(
            '--dtsph-cs', action='store', type=float, dest='cs',
            default=None,
            help='Speed of sound in pseudo time.'
        )
        group.add_argument(
            '--alpha', action='store', type=float, dest='alpha',
            default=None,
            help='Artificial viscosity.'
        )
        add_bool_argument(
            group, 'dtsph-ghost', dest='has_ghosts', default=None,
            help="Advection of pressure and velocity in pseudo time."
        )
        add_bool_argument(
            group, 'edac', dest='edac', default=None,
            help="Use EDAC correction."
        )

    def consume_user_options(self, options):
        vars = ['alpha', 'has_ghosts', 'cs', 'edac']
        data = dict((var, self._smart_getattr(options, var))
                    for var in vars)
        self.configure(**data)

    def _get_edac_nu(self):
        art_nu = 0.5*self.h*self.cs/8
        if art_nu > 0:
            nu = art_nu
            print("Using artificial viscosity for EDAC with nu = %s" % nu)
        else:
            nu = self.nu
            print("Using real viscosity for EDAC with nu = %s" % self.nu)
        return nu

    def configure_solver(self, kernel=None, integrator_cls=None,
                         extra_steppers=None, **kw):
        from pysph.base.kernels import QuinticSpline
        from pysph.solver.solver import Solver
        if kernel is None:
            kernel = QuinticSpline(dim=self.dim)

        steppers = {}
        if extra_steppers is not None:
            steppers.update(extra_steppers)

        for fluid in self.fluids:
            if fluid not in steppers:
                steppers[fluid] = DTSPHSteadyStep()

        cls = (DTSPHSteadyIntegrator if integrator_cls is None
               else integrator_cls)
        integrator = cls(**steppers)

        self.solver = Solver(
            dim=self.dim, integrator=integrator, kernel=kernel, **kw
        )

    def get_equations(self):
        from pysph.sph.wc.viscosity import LaminarViscosity
        from pysph.sph.wc.transport_velocity import SetWallVelocity
        groups = []
        all = list(self.fluids) + list(self.solids)

        edac_nu = self._get_edac_nu() if self.edac else None

        eqs = [SetWallVelocity(dest=s, sources=self.fluids)
               for s in self.solids]
        if eqs:
            groups.append(Group(equations=eqs))

        eq0 = []
        for fluid in self.fluids:
            eq0.extend([
                MomentumEquationPressureGradient(
                    dest=fluid, sources=all, c0=self.cs,
                    alpha=self.alpha, beta=0.0,
                    gx=self.gx, gy=self.gy, gz=self.gz
                ),
                PredictPressure(
                    dest=fluid, sources=all, cs=self.cs
                )
            ])
            if self.edac:
                eq0.append(
                    PredictPressureEDAC(
                        dest=fluid, sources=all, cs=self.cs, nu=edac_nu
                    )
                )
            eq0.append(
                AdvectVelocityPressure(dest=fluid, sources=all)
            )
            if self.nu > 0.0:
                eq0.append(
                    LaminarViscosity(
                        dest=fluid, sources=self.fluids, nu=self.nu
                    )
                )
                if self.solids:
                    eq0.append(
                        SolidWallNoSlipBC(dest=fluid, sources=self.solids,
                                          nu=self.nu)
                    )

            for solid in self.solids:
                eq0.append(
                    PredictPressure(
                        dest=solid, sources=self.fluids, cs=self.cs
                    )
                )

        sg0 = Group(equations=eq0)
        groups.append(sg0)

        if self.has_ghosts:
            groups.append(Group(
                equations=[
                    UpdateGhostProps(dest=x, sources=None) for x in self.fluids
                ], real=False
            ))

        equations = groups
        return equations

    def setup_properties(self, particles, clean=True):
        dummy = get_particle_array_dtsph()
        props = set(dummy.properties.keys())
        constants = [dict(name=x, data=v) for x, v in dummy.constants.items()]
        for pa in particles:
            self._ensure_properties(pa, props, clean)
            pa.set_output_arrays(dummy.output_property_arrays)
            for const in constants:
                pa.add_constant(**const)
        particle_arrays = dict([(p.name, p) for p in particles])
        if self.has_ghosts:
            for fluid in self.fluids:
                pa = particle_arrays[fluid]
                pa.add_property('orig_idx', type='int')

        solid_props = ['wij', 'ug', 'vg', 'wg', 'uf', 'vf', 'wf']
        for solid in self.solids:
            pa = particle_arrays[solid]
            for prop in solid_props:
                pa.add_property(prop)
