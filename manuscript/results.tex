\section{Results and discussion}
\label{sec:results}

In this section we perform various numerical experiments using standard
benchmark problems. We explore the following specific questions using the
Taylor-Green problem which has an exact solution:
\begin{itemize}
\item Is it worth moving the particles in pseudo-time or can we freeze the
  particles? This has significant performance implications.
\item What possible values of $\beta$ can be used?
\item What suitable values of the tolerance $\epsilon$ can be chosen and what
  does this imply for accuracy?
\end{itemize}

Once these are explored, a suite of test problems are simulated with the DTSPH
scheme and compared with other schemes like the standard
WCSPH~\cite{sph:fsf:monaghan-jcp94}, transport velocity formulation
(TVF)~\cite{Adami2013}, Entropically damped artificial compressibility (EDAC)
~\cite{edac-sph:cf:2019}, and the $\delta$-SPH
scheme~\cite{antuono-deltasph:cpc:2010}. Except where noted, the DTSPH scheme
uses the EDAC formulation in all the simulations below i.e.\ the pressure
evolution uses equation~\eqref{eq:dp-dtau-frozen-edac}.

The TVF, EDAC, $\delta$-SPH, and WCSPH schemes are part of the
PySPH~\cite{pysph2020,PR:pysph:scipy16} framework. All the results presented below
are automated and the code for the benchmarks is available at
\url{https://gitlab.com/pypr/dtsph}. The tools used to automate the results
are described in detail in~\cite{pr:automan:2018}. This allows us to
automatically reproduce every figure and table in this manuscript.

All the simulations are performed on a four core Intel (R) Core (TM) i$5-7400$
CPU with a clock speed of 3.00GHz. The problems are executed on four cores
using OpenMP.  We use the DTSPH scheme along with the EDAC in all results
below unless explicitly mentioned otherwise.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Taylor-Green problem}
\label{sec:tgv}

The Taylor-Green problem is a classical problem which is periodic along both
$x$ and $y$ axes and has an exact solution. This is a particularly challenging
problem for SPH~\cite{Adami2013, sph:acisph:cpc:2017, fatehi-2019} since the
particles move along the streamlines towards a stagnation point leading to
particle disorder.

The exact solution for the Taylor-Green problem is given by
%
\begin{align}
  \label{eq:tgv_sol}
  u &= - U e^{bt} \cos(2 \pi x) \sin(2 \pi y), \\
  v &=   U e^{bt}\sin(2 \pi x) \cos(2 \pi y), \\
  p &=  -U^2 e^{2bt} (\cos(4 \pi x) + \cos(4 \pi y))/4,
\end{align}
where $U$ is chosen as $1m/s$, $b=-8\pi^2/Re$, $Re=U L /\nu$, and $L=1m$.

The Reynolds number $Re$ is set to 100 and various cases are tested to better
understand the scheme. For all the simulations, the quintic spline kernel is
used with $h/\Delta x = 1.0$, no artificial viscosity is used. The following
cases are considered,
\begin{itemize}
\item a comparison of results when particles are either advected or frozen in
  pseudo-time;
\item the effect of changing the artificial speed of sound, $\beta$;
\item the effect of changing the convergence tolerance, $\epsilon$;
\item comparison of results with different schemes; and
\item comparison of the results with different number of particles and with
  different Reynolds numbers.
\end{itemize}
%
The results are compared against the exact solution. Particle plots are also
shown wherever necessary as the error plots do not always reveal any particle
disorder, particle clumping, or voiding occurring in the flow. In addition, we
compare the performance of the schemes where the difference is noticeable.

We first discuss a simple method of initializing the particles that we use to
compare all the schemes in a consistent and fair manner.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Perturbation in initial positions}
\label{sub:sec:tg:perturb}

As discussed earlier, it is not always easy to obtain good results for the
Taylor-Green problem. When the particles are initially distributed in a
uniform manner, they tend to move towards the stagnation points and this often
leads to severe particle disorder. In~\cite{edac-sph:cf:2019}, it was found
that this problem can be reduced by introducing a small amount of noise in the
initial particle distribution. To this end, a small random displacement is
given to the particles with a maximum displacement of $\Delta x /10$. The
random numbers are drawn from a uniform distribution and the random seed is
kept fixed leading to the same distribution of particles for all cases with
the same number of particles. This allows us to perform a fair comparison.
Initially the particles are arranged in a $100 \times 100$ grid, with
smoothing length of $h/\Delta x = 1$, $\Delta t = 0.00125$, $\epsilon =
10^{-4}$, and simulated for $2.5 s$ with $\beta=5$.

The decay rate is computed by computing the magnitude of maximum velocity
$|\ten{V}_{\max}|$ at each time step, the $L_1$ error is computed as the
average value of the difference between the exact velocity magnitude and the
computed velocity magnitude, given as
%
\begin{equation}
  \label{eq:tg:l1}
  L_1 = \frac{\sum_i |\ten{V}_{i, computed} - \ten{V}_{i, exact}|}
  {\sum_i |\ten{V}_{i, exact}|},
\end{equation}
%
where $\ten{V}_i$ is computed at the particle positions for each particle $i$
in the flow.
%
\begin{figure}[!h]
  \centering
  \begin{subfigure}{0.48\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{figures/taylor_green/perturb/pert_decay_all}
    \subcaption{Decay of maximum velocity with time.}\label{fig:tg:pert:decay}
  \end{subfigure}
%
  \begin{subfigure}{0.48\textwidth}
    \centering
    \includegraphics[width=1.0\textwidth]{figures/taylor_green/perturb/pert_l1_error_all}
    \subcaption{$L_1$ error of velocity magnitude with time.}\label{fig:tg:pert:l1}
  \end{subfigure}
  \caption{Comparison of perturbation (of atmost $\Delta x/10$) and without any
    perturbation.}
\label{fig:tg:pert}
\end{figure}
%
Fig.~\ref{fig:tg:pert:decay} shows the decay of the velocity compared with the
exact solution for the case where there is no initial perturbation and with a
small amount of perturbation (of at most $\Delta x/10$). As can be clearly
seen, the introduction of the perturbation significantly improves the results.
This is clearly seen in the $L_1$ norm of the error in the velocity magnitude
in Fig.~\ref{fig:tg:pert:l1}. While we have not shown this, the results are
similar for simulations made using most other schemes including the new
scheme, TVF, and the EDAC. Given this, we henceforth use a small initial
perturbation for the results for the Taylor-Green vortex problem.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Advection of particles in pseudo-time}
\label{sub:sec:tg:advect}

As discussed earlier, it is important to study the effect of moving the
particles in pseudo-time as against keeping them frozen in pseudo-time.  If we
find that there is no significant advantage gained by moving the particles in
pseudo-time, we can simplify the implementation of the scheme as well as
improve its performance considerably.

We consider the Taylor Green problem and compare the results of simulations
where we advect the particles and where we hold them frozen. The rest of the
parameters are held fixed. The same small perturbation is added to the initial
particle position. Parameters used for this simulation are, initial particle
spacing $\Delta x = \Delta y = 0.01$, Reynolds number $Re=100$, the value of
$\beta=5$, time step $\Delta t = 0.00125$, and tolerance of $\epsilon =
10^{-6}$.
\begin{figure}[!h]
  \centering
  \begin{subfigure}[!h]{0.48\textwidth}
    \includegraphics[width=\linewidth]{figures/taylor_green/advect/pert_decay_all}
    \subcaption{Decay of maximum velocity with time.}\label{fig:tg:advect:decay}
  \end{subfigure}
%
  \begin{subfigure}[!h]{0.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{figures/taylor_green/advect/pert_l1_error_all}
    \subcaption{$L_1$ error in the velocity magnitude with time.}\label{fig:tg:advect:l1}
  \end{subfigure}
    \caption{Velocity decay plot for $Re = 100$ for both advection and no
      advection of particles in pseudo time as compared with the exact
      solution for $Re = 100$.}\label{fig:tg:advect}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \begin{subfigure}[!h]{0.48\textwidth}
    \includegraphics[width=\linewidth]{figures/taylor_green/nnps/pert_decay_all}
    \subcaption{Decay of maximum velocity with time.}\label{fig:tg:advect:no_nnps:decay}
  \end{subfigure}
%
  \begin{subfigure}[!h]{0.48\textwidth}
    \centering
    \includegraphics[width=\linewidth]{figures/taylor_green/nnps/pert_l1_error_all}
    \subcaption{$L_1$ error in the velocity magnitude with time.}\label{fig:tg:advect:no_nnps:l1}
  \end{subfigure}
  \caption{Comparison for decay rates with time and $L_1$ errors in velocity
for advection and without advection cases while no update in the neighbour
particles.}
\label{fig:tg:advect:no_nnps}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{figures/taylor_green/advect/comparision_vmag.pdf}
  \caption{Particle plots for the Taylor-Green problem with advection and
    without advection in pseudo time while updating the
    neighbours.  Plots are shown at $t=2.5s$ with $Re=100$.}
  \label{fig:tg:advect:particle_plots}
\end{figure}

We recall that when we advect the particles in pseudo-time, we need to update
the neighbors, however the displacements are very small and this is not
necessary. We perform simulations to see if the differences are significant.
Fig.~\ref{fig:tg:advect:decay} shows the decay rate for the case with and
without advection in pseudo-time while updating the neighbours. There are no
noticeable differences in the results and the plots for each case lie on each
other. This is also seen in Fig.~\ref{fig:tg:advect:l1} which shows the $L_1$
error. Fig.~\ref{fig:tg:advect:no_nnps:decay} and
Fig.~\ref{fig:tg:advect:no_nnps:l1} show the decay rate and the $L_1$ error in
the velocity magnitude while not updating the neighbours resulting in a
similar conclusion that movement of particles in pseudo-time is too small to
significantly influence the results.

\begin{table}[!h]
\centering
\input{figures/taylor_green/advect/taylor_green.tex}
\caption{CPU time taken for $2.5$ secs of Taylor-Green simulation with
  $100 \times 100$ particles, with advection and without advection in pseudo
  time. }
\label{table:tg:advect:times}
\end{table}
%
While the accuracy is unaffected, the performance is significantly different
as can be seen from Table~\ref{table:tg:advect:times}. This shows that
advection of particles reduces performance by close to a factor of two. This
increase in performance is largely due to the fact that we re-calculate the
neighbour particles when we advect them. There is also some increase due to
the additional computations required for the advection.
Fig.~\ref{fig:tg:advect:particle_plots} shows the particle plots with color
representing velocity magnitude for the case where the particles are advected
and frozen. The results look identical. Based on these results, we do not
advect the particles in pseudo-time for any of the other simulations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{The influence of $\beta$}
\label{sub:sec:tg:dtaufac}

The parameter $\beta$ is the ratio of $c/V_{ref}$, as discussed earlier. The
pseudo-timestep is also determined such that $\Delta t = \beta \Delta \tau$.
In this section we consider the Taylor-Green problem simulated at $Re=100$
using $100 \times 100$ particles, using the new scheme with different values of
$\beta$ chosen between 2 and 20 for a tolerance $\epsilon = 10^{-4}$, and run
for a simulation time of $t = 1$ sec.

Figs.~\ref{fig:tg:beta:decay} and~\ref{fig:tg:beta:l1} show the decay rate and
the $L_1$ error in the velocity for the different cases. From these it appears
that $\beta$ between 5 -- 20 works well.  We use a value of $\beta=10$ in all
simulations unless explicitly mentioned.
%
\begin{figure}[!h]
  \centering
  \begin{subfigure}[b]{0.48\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/beta/pert_decay_all}
    \subcaption{Decay of maximum velocity with time.}\label{fig:tg:beta:decay}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.48\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/beta/pert_l1_error_all}
    \subcaption{$L_1$ error of velocity magnitude with time.}\label{fig:tg:beta:l1}
  \end{subfigure}
  \caption{Decay rate of the maximum velocity and the $L_1$ error in the
    velocity magnitude for $\beta$ values of $[2, 5, 10, 20]$ for the DTSPH
    scheme with an error tolerance of $10^{-4}$.}\label{fig:tg:beta}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Changing the convergence tolerance parameter $\epsilon$}
\label{sub:sec:tg:tol}

We next choose $\beta=10$ and vary the tolerance from $10^{-2}$ to $10^{-5}$.
Fig.~\ref{fig:tg:tol:decay} shows the decay rates as the tolerance is changed
and Fig.~\ref{fig:tg:tol:l1} shows the $L_1$ error in the velocity. These
results clearly show that reducing the tolerance improves the accuracy.
However, we do see that the solutions are by-and-large robust to changes in
$\epsilon$ over a very large range. As expected, increase in the tolerance
leads to increase in the simulation time taken as seen in
Table.~\ref{table:tg:tol:times}.
%
\begin{table}[!htb]
\centering
\input{figures/taylor_green/tol/taylor_green.tex}
\caption{CPU time time taken for a simulation time of $2.5$ secs with $100 \times 100$
  particles with varying tolerance.}\label{table:tg:tol:times}
\end{table}
%
\begin{figure}[!h]
  \centering
  \begin{subfigure}[b]{0.48\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/tol/pert_decay_all}
  \subcaption{Decay of maximum velocity with time.}\label{fig:tg:tol:decay}
  \end{subfigure}
%
  \begin{subfigure}[b]{0.48\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/tol/pert_l1_error_all}
  \subcaption{$L_1$ error in the velocity magnitude with time.}\label{fig:tg:tol:l1}
\end{subfigure}
\caption{Comparision for various tolerance $(\epsilon)$ ranging from
  $10^{-2}$ to $10^{-5}$.}
\end{figure}
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Varying Reynolds number}
\label{sub:sec:tg:re}

We simulate the problem at $Re=1000$ using the most appropriate parameters
based on the previous results. The DTSPH scheme is used with a quintic spline
kernel, maximum initial random particle displacement of $\Delta x/ 10$, with a
tolerance of $\epsilon = 10^{-3}$, and $\beta = 5$ for different initial
particle arrangement of $25 \times 25$ to $200 \times 200$ and simulated for
$2.5$ secs.

Fig.~\ref{fig:tg:re:100} shows the maximum velocity decay as well as the $L_1$
error of the velocity magnitude for the case of $Re=100$.
Fig.~\ref{fig:tg:re:1k} shows the same for $Re=1000$. It can be seen that in
the case of $Re=1000$ that there is a clear reduction in the errors as the
resolution is increased. In the case of $Re=100$, it appears that the errors
are lower for the $50 \times 50$ resolution and increase by a small amount as
the resolution is increased. We believe that this occurs because of possible
issues with the convergence of the discretization of the diffusive terms in
the governing equations. We point out that at $Re=500$ we obtain similar
convergence as in the case of $Re=1000$ showing that issue is not with the
convergence of the DTSPH scheme per-se. These results show that the new scheme
performs very well.

\begin{figure}[!h]
  \centering
  \begin{subfigure}[b]{\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/re/re_100}
  \subcaption{Decay of maximum velocity with time and the $L_1$ error in the
    velocity magnitude for $Re = 100$.}\label{fig:tg:re:100}
  \end{subfigure}

  \begin{subfigure}[b]{\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/re/re_1000}
  \subcaption{Decay of maximum velocity with time and the $L_1$ error in the
    velocity magnitude for $Re = 1000$.}\label{fig:tg:re:1k}
  \end{subfigure}
  \caption{Comparison of results for the Taylor-Green problem using five
    different particle configuration between $25 \times 25$ to $200 \times
    200$. Shown are the results for the Reynolds number of $Re = 100$ and
    $1000$.}\label{fig:tg:re}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Comparision with other schemes}
\label{sub:sec:tg:compare}

Here we simulate the problem for $t = 2.5s$ for $Re = 100$ using the new DTSPH
scheme comparing it with WCSPH, $\delta$-SPH, and EDAC. The quintic spline
kernel is used for all the schemes with $h = \Delta x$. For all the cases the
particles are perturbed by atmost $\Delta x / 10$. For DTSPH, we use
$\beta = 10$ with a tolerance of $\epsilon = 10^{-4}$. We use an initial
configuration of $100 \times 100$ particles.

As can be seen from the results shown in Fig.~\ref{fig:tg:compare}, the new
scheme is more accurate than the standard WCSPH scheme. The scheme is more
accurate than the standard $\delta$-SPH scheme. We note that we do not employ
any form of shifting for the $\delta$-SPH and WCSPH scheme cases. The scheme
is not more accurate than the EDAC scheme as in the EDAC scheme, the particles
are also regularized using the transport velocity formulation which
significantly improves the results. As can be seen from the
Table~\ref{table:tg:compare:times}, the new scheme is anywhere from 1.9 to 2.8
times faster than the other schemes.

In Fig.~\ref{fig:tg:streamplot}, we show the streamlines as well as the
particle plot with the color indicating pressure at $t=2.5$secs for the DTSPH
case for $Re=100$.  As can be seen the particle distribution is smooth.
%
\begin{table}[!htb]
\centering
\input{figures/taylor_green/scheme/taylor_green.tex}
\caption{CPU time time taken for a simulation time of $2.5$ secs with $100 \times 100$
  particles for various schemes.}\label{table:tg:compare:times}
\end{table}

\begin{figure}[!h]
  \centering
  \begin{subfigure}[b]{0.48\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/scheme/pert_decay_all}
  \subcaption{Decay of maximum velocity with time.}\label{fig:tg:re:decay}
  \end{subfigure}
  %
  \begin{subfigure}[b]{0.48\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/taylor_green/scheme/pert_l1_error_all}
  \subcaption{$L_1$ error in the velocity magnitude with time.}\label{fig:tg:re:l1}
  \end{subfigure}
  \caption{Comparision of DTSPH with other schemes for the simulation of
    Taylor-Green problem, with $Re = 100$ and using $100 \times 100$
    particles.}\label{fig:tg:compare}
\end{figure}

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{figures/taylor_green/tg_streamplot}
  \caption{Particle plots for the Taylor-Green problem, with $100 \times 100$
    particles in the initial configuration, showing streamlines on the
    left and pressure on the left at $t = 2.5s$.}%
  \label{fig:tg:streamplot}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{The lid-driven-cavity problem}
\label{sec:ldc}

We next consider the classic lid-driven-cavity problem. This is a fairly
challenging problem to simulate with SPH~\cite{Adami2013, ghasemi-2013,
  lee-comparison-2008}. The fluid is placed in a unit square with a lid moving
with a unit speed to the right. The bottom and side walls are treated as
no-slip walls. The Reynolds number of the problem is given by
$Re=\frac{V}{\nu}$, where $V$ is the lid velocity. We use a quintic spline
kernel with $h=\Delta x$. The problem is simulated at $Re=100$ using a
$50\times 50$, $100\times 100$, and $150 \times 150$ grid for a simulation
time of $t = 10s$ until there is no change in the kinetic energy of the
system. For the DTSPH scheme we use a $\beta = 10$ with a tolerance
$\epsilon=10^{-4}$. The results are compared with those of the TVF
scheme\cite{Adami2013} and the established results of Ghia et
al.~\cite{ldc:ghia-1982}.
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{figures/cavity/uv_re100}
  \caption{Velocity profiles $u$ vs.\ $y$ and $v$ vs.\ $x$ for the
    lid-driven-cavity problem at $Re=100$ with three initial particle
    arrangement of $50 \times 50$, $100 \times 100$, and $150 \times
    150$. Here we compare DTSPH with TVF and the results
    of~\cite{ldc:ghia-1982}.}\label{fig:ldc:uv_re100}
\end{figure}
%
Fig.~\ref{fig:ldc:uv_re100}, shows the centerline velocity profiles for $u$
vs.\ $y$ and $v$ vs.\ $x$ for different resolutions of particles. It is seen
that the TVF scheme produces better results as expected. However, the results
of the new scheme are in good agreement. In Fig.~\ref{fig:ldc:streamplot}, we
show the particle distribution with the velocity magnitude on the left and
streamlines on the right.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\linewidth]{figures/cavity/streamplot}
  \caption{Particle plots for the cavity problem, with $150 \times 150$
    particles in the initial configuration, showing the velocity magnitude on
    the left and streamlines on the right at $t = 10s$.}%
  \label{fig:ldc:streamplot}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Steady Lid-driven cavity}
\label{sec:steady_ldc}

In order to show that we are able to obtain steady state results, we employ
the steady state equations discussed in Section~\ref{sec:steady-state} to
solve the lid-driven-cavity problem. We solve the problem until there is no
change in the kinetic energy of the system. We simulate the problem using a
quintic spline kernel for $Re=100$ and $Re=1000$ using a $50\times 50$,
$100\times 100$ and $150 \times 150$ particle grid. For $Re=100$ we simulate
the problem up to $\tau=10$ and for $Re=1000$ we simulate up to $\tau=50$.
%
\begin{figure}[!h]
  \centering
  \includegraphics[scale=0.5]{figures/steady_cavity/uv_re100}
  \caption{Velocity profiles for the lid-driven-cavity using the steady state
    simulation procedure for $Re = 100$ with initial partial arragement of
    $50 \times 50$, $100 \times 100$, and $150 \times 150$ compared with the
    results of~\cite{ldc:ghia-1982}.}
\label{fig:steady:ldc:uv_re100}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.4\linewidth]{figures/steady_cavity/uv_re1000}
  \caption{Velocity profiles for the lid-driven-cavity using the steady state
    simulation procedure for $Re = 1000$ with initial partial arragement of
    $50 \times 50$, $100 \times 100$, and $150 \times 150$ compared with
    the results of~\cite{ldc:ghia-1982}.}
\label{fig:steady:ldc:uv_re1000}
\end{figure}
%
Fig.~\ref{fig:steady:ldc:uv_re100} shows the velocity profiles for the
$Re=100$ case and Fig.~\ref{fig:steady:ldc:uv_re1000} shows velocity profiles
for the $Re = 1000$ case.

These results show that we are able to simulate internal flows very well using
the new DTSPH scheme. We have also demonstrated that the steady-state
equations also work very well. We next consider problems that involve a
free-surface.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Square patch}
\label{sec:sp}

The square patch problem~\cite{colagrossi-phdthesis:2005, khayyer-2013,
  sun-deltap-2017} is a free surface problem where a square patch of fluid of
side $L$ is subjected to the following initial conditions,
%
\begin{equation}
  \begin{aligned}
    u_{0}(x, y) &= \omega y, \\
    v_{0}(x, y) &= -\omega x,
  \end{aligned}
\end{equation}
%
\begin{equation}
  p_{0}(x, y) = \rho \sum_m^\infty \sum_n^\infty -\frac{32\omega^2/(m n \pi^2)}
  {\left[{\left(\frac{n\pi}{L}\right)}^2 + {\left(\frac{m \pi^2}{L}\right)}^2
    \right]} \sin\left(\frac{m \pi x^*} {L}\right)\sin\left(\frac{n \pi y^*}
    {L}\right) m, n \in \mathbb{N}_{odd},
\end{equation}
%
where $X^* = x + L/2$ and $y^* = y + L/2$.

We simulate this problem for $t = 3s$ using the DTSPH, and EDAC schemes for
comparison. In this case, the EDAC simulations also employ particle
shifting~\cite{diff_smoothing_sph:lind:jcp:2009}. The quintic spline kernel
with $h/\Delta x = 1.3$ is used for all the schemes, artificial viscosity
$\alpha = 0.1$ is used for all the schemes. For the DTSPH scheme, $\beta =
10$~with a tolerance $\epsilon = 10^{-3}$ is used. Two different initial
configurations of $50 \times 50$ and $200 \times 200$ particles are used.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\textwidth]{figures/square_patch/square_patch}
  \caption{Particle distribuiton plots at $t = 3$~secs for the square patch
    problem. Artificial viscosity is used in all the schemes. Top row
    corresponds to $50 \times 50$ particles, and the bottom row corresponds to
    $100 \times 100$ particles. In column (a) EDAC scheme is used, with
    $200 \times 200$ particles, column (b) indicates DTSPH scheme with a
    tolerance of $\epsilon = 10^{-3}$, and $50 \times 50$ particles, and in
    column (c) DTSPH scheme is used with a tolerance of $\epsilon = 10^{-3}$,
    and $200 \times 200$ particles.}
\label{fig:sp:particle_plots}
\end{figure}
%
The particle distribution for each scheme at the end of $t=3s$ is shown in
Fig.~\ref{fig:sp:particle_plots}. The plots of DTSPH and EDAC are in good
agreement with each other showing that the new scheme is as good as the EDAC
scheme.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Elliptical drop}
\label{sec:ed}

The elliptical drop problem was first solved in the context of the SPH by
Monaghan~\cite{sph:fsf:monaghan-jcp94}. This problem is also solved in the
context of truly incompressible SPH~\cite{khayyer-2013, lind-2016,
  rezavand-2018a}. In this problem an initially circular drop of inviscid
fluid having unit radius is subjected to the initial velocity field given by
$-100x\hat{i} + 100 y \hat{j}$. The outer surface is treated as a free
surface. Due to the incompressibility constraint on the fluid there is an
evolution equation for the semi-major axis of the ellipse.

This problem is simulated using the DTSPH, $\delta$-SPH (without shifting),
and EDAC (with shifting) respectively. An artificial viscosity parameter of
$\alpha = 0.15$ is used for all the schemes. An error tolerance of $\epsilon =
10^{-4}$ is used for the DTSPH and scheme. $\beta=10$, $\Delta x=0.02$, $h=1.3
\Delta x$ and a quintic spline kernel is used for all the schemes. The
simulation is run for $t = 0.0076s$.

Fig.~\ref{fig:ed:particle_plots} shows the distribution of particles for
different schemes. The colors indicate the pressure. As can be seen, the
DTSPH and EDAC results are similar. It is important to note that all
the pressure values are in a similar range with none of the schemes exhibiting
severe noise in the pressure.
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.8\textwidth]{figures/elliptical_drop/elliptical_drop_p}
  \caption{The distribution of particles for the elliptical drop problem at $t
    = 0.0076$ seconds. The plot (a) is with the EDAC using $200\times 200$
    particles. Plot (b) is that of the new DTSPH scheme with $50 \times 50$
    particles, (c) uses DTSPH with $200 \times 200$. The solid blue line is
    the exact solution for the shape of the drop and the colors indicate the
    pressure.}
\label{fig:ed:particle_plots}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\linewidth]{figures/elliptical_drop/ke}
  \caption{The kinetic energy with time of the Elliptical drop problem as
computed with DTSPH, $\delta$-SPH, and EDAC schemes.}
\label{fig:ed:ke}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\linewidth]{figures/elliptical_drop/major_axis}
  \caption{Error in computed size of semi-major axis of the elliptical drop
    problem compared with the exact solution for the DTSPH, $\delta$-SPH and EDAC schemes.}
\label{fig:ed:major_axis}
\end{figure}
%
Fig.~\ref{fig:ed:ke}, shows the evolution of the kinetic energy, the results
are similar for DTSPH and EDAC schemes. This is to be expected. However it is
interesting to note that the kinetic energy of the $\delta$-SPH scheme decays
faster than the other schemes. Fig.~\ref{fig:ed:major_axis} shows the error in
the semi-major axis as compared to the exact solution. The DTSPH and EDAC both
perform slightly better than the $\delta$-SPH scheme. The results indicate
that the new scheme performs well in comparison with state of the art
weakly-compressible schemes.

\subsection{Dam-break in 2 dimensions}
\label{sec:db2d}

A two dimensional dam-break over a dry bed~\cite{lee-comparison-2008,
  marrone-deltasph:cmame:2011, lind-2016} is considered next. The DTSPH and the
standard EDAC schemes are compared. The simulation is performed for $1s$.  The
quintic spline kernel is used with $h/\Delta x = 1.0$, and an artificial
viscosity of $\alpha = 0.1$ is used for all the schemes. A tolerance of
$\epsilon=10^{-4}$ is used for DTSPH.

The problem considered is described in~\cite{lee_violeau:db3d:jhr2010} with a
block of fluid column of height $h = 2m$, width $w = 1m$. The block is
released under gravity which is assumed to be $-9.81 m/s^2$.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\linewidth]{figures/dam_break_2d/db2d_p.pdf}
  \caption{Particle distribution plots with color indicating pressure for the
    dam-break 2D problem at various times. DTSPH is shown on the left, and
    EDAC is shown on the right. Top row is at $t = 0.4$ secs, second row is at
    $t = 0.6$ secs, and bottom row is at $t = 0.8$ secs.}
\label{fig:db:particle_plots:pre}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[!h]
  \centering
  \includegraphics[width=1.0\linewidth]{figures/dam_break_2d/db2d_vmag.pdf}
  \caption{Particle distribution plots with color indicating velocity
    magnitude for the 2D dam-break problem at variuos times. DTSPH scheme is
    shown on the left, and EDAC is shown on the right. Top row is at $t = 0.4$
    secs, the second row is at $t=0.2$ secs, and bottom row is at $t = 0.8$
    secs.}
\label{fig:db:particle_plots:vmag}
\end{figure}
%
\begin{figure}[!h]
  \centering
  \includegraphics[width=0.6\linewidth]{figures/dam_break_2d/toe_vs_t.pdf}
  \caption{Position of the toe of the dam versus time of DTSPH, WCSPH,
    $\delta$-SPH and EDAC, as compared with the simulation
    of~\cite{koshizuka_oka_mps:nse:1996}. $Z$ is the distance of toe of the
    dam from the left wall and $L$ is the initial width of the dam}
\label{fig:sp:wcsph_toe_vs_t}
\end{figure}
%
For the DTSPH and EDAC schemes, the particle distribution is shown in
Fig.~\ref{fig:db:particle_plots:pre} at various times with color indicating
pressure. Fig.~\ref{fig:db:particle_plots:vmag} shows the particle
distribution with color indicating velocity magnitude at various times. The
results of the new scheme seem largely comparable with that of the EDAC
scheme. The results also show the improvements obtained by the addition of
diffusive term in the pressure evolution equation. This significantly reduces
the noise. The standard EDAC results are very similar to those of the
$\delta$-SPH and are hence not shown. Fig.~\ref{fig:sp:wcsph_toe_vs_t} plots
the position of the toe of the dam versus time as compared with the results of
the Moving Point Semi-implicit scheme of~\cite{koshizuka_oka_mps:nse:1996}.
The results of the DTSPH scheme are in good agreement with those of the EDAC
scheme.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Dam-break in three dimensions}
\label{sec:db3d}

A three dimensional case is shown to demonstrate the performance of the new
scheme as compared to the EDAC scheme. This is an important case as the
previous problems only require a smaller number of particles. We consider a
three-dimensional dam break over a dry bed with an obstacle. A cubic spline
kernel is used for both the new scheme and the EDAC with $h/\Delta x = 1.3$
and artificial viscosity $\alpha=0.1$. The problem is simulated for a total
time of 1 second.  We do not use any particle shifting in this case.

The problem considered is described in~\cite{lee_violeau:db3d:jhr2010} with a
block of fluid column of height $h = 0.55m$, width $w = 1.0m$ and length $l =
1.228m$. The container is $3.22m$ long. The block is released under gravity
with an acceleration of $-9.81 m/s^2$. Both schemes are simulated with a fixed
time step. For both schemes we use a CFL of 0.25. The speed of sound for the
EDAC case is set to $10 \sqrt{2gh}$, where $h$ is the height of the water
column. For the DTSPH, we use a time step of $\frac{0.25h}{\sqrt{2gh}}$, and
use $\beta=10, \epsilon=10^{-3}$. The particle spacing, $\Delta x=0.02$,
leading to around 240000 particles in the simulation.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
\begin{figure}[!h]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/edac_u_0}
  \end{subfigure}
%
  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/dtsph_edac_tol_1e-4_u_0}
  \end{subfigure}

  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/edac_u_1}
  \end{subfigure}
%
  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/dtsph_edac_tol_1e-4_u_1}
  \end{subfigure}

  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/edac_u_2}
    \caption{EDAC scheme.}
  \end{subfigure}
%
  \begin{subfigure}{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{figures/dam_break_3d/dtsph_edac_tol_1e-4_u_2}
    \caption{DTSPH scheme.}
  \end{subfigure}
  \caption{Dam-break 3D particle distribution at various times, first row is
    at $t = 0.4$~secs second row is at $t = 0.6$~secs and third row is at $t =
    1.0$~secs. The left column is EDAC scheme and right column is the DTSPH
    scheme.}
\label{fig:db3d:particle_plots_50}
\end{figure}

Fig.~\ref{fig:db3d:particle_plots_50} shows the particle distribution at
various times for both the schemes. This indicate that the new scheme produces
good results. Table~\ref{table:db3d:compare:times} shows the time taken for
the different 3D dam break simulations. Depending on the tolerance chosen, we
are able to obtain between a 1.7 to 7.16 fold improvement in performance as
compared to the EDAC scheme. We note that as the tolerance $\epsilon$ is
reduced, the DTSPH requires more iterations in pseudo-time in order to attain
convergence and this reduces the performance. However even with lower
tolerance values we are able to obtain very good results. In the next section
we demonstrate the performance achievable with the new scheme for different
problems.

\begin{table}[!htb]
\centering
\input{figures/dam_break_3d/dam_break_3d.tex}
\caption{CPU time time taken for different simulations of the 3D dam break
  problem.}\label{table:db3d:compare:times}
\end{table}


\subsection{Performance}
\label{dtsph:performance}

The performance of DTSPH is compared with that of other schemes for various
problems.  In Table~\ref{table:compare:times} we list the different problems
and the speedup obtained by using the new scheme.

As we have seen before, for the dam-break problem in three dimensions it can
be seen that DTSPH (with a tolerance of $10^{-3}$) can be up to 7.16 times
faster than the standard EDAC scheme. When a very low tolerance is used, the
scheme is about 1.7 times faster this is only to be expected as lower
tolerances require many more iterations to obtain a converged pressure. For
the two-dimensional dam break cases, the new scheme is about 3.5 times faster
than the EDAC or the $\delta$-SPH. For the unsteady cavity problem at $Re=100$
with a $150\times 150$ grid of particles, we get up to a 7 times performance
improvement. This is quite significant since in these cases we have compared
these cases where the effective Mach number is 0.1. Given this, the primary
advantage with the DTSPH is that it can take a time step that is 10 times
smaller. Hence in this case a speed-up of close to 7 suggests that it is an
efficient scheme.

\begin{table}[!htb]
\centering
\begin{tabular}{llr}
\toprule
Problem &  Scheme & Speed up\\
\midrule
Dam break 3D &   EDAC vs. DTSPH ($\epsilon = 10^{-3}$) &  7.16 \\
Dam break 3D &   EDAC vs. DTSPH ($\epsilon = 10^{-4}$) &  1.68 \\
Dam break 2D &   EDAC vs. DTSPH & 3.57 \\
  Dam break 2D &   EDAC vs. $\delta$-SPH & 3.66 \\
Cavity & TVF vs DTSPH & 6.87 \\
\bottomrule
\end{tabular}
\caption{Speed-up obtained for different simulations when using the DTSPH
  scheme.}
\label{table:compare:times}
\end{table}


The suite of benchmarks considered shows that the new scheme is robust,
simulates a variety of problems, and is as accurate as the EDAC scheme. In
addition it is very efficient and can be as much as seven times faster than
the EDAC scheme. Indeed, it is possible to improve the performance even more
by caching the values of the kernel and kernel gradients during the
pseudo-time iterations but we have not done this in the present work.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% fill-column: 78
%%% End:
